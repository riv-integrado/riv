## RIV - Reserva Inteligente de Vagas

## Introdução

Bem-vindo ao RIV, um sistema em Flask projetado para facilitar o uso e o gerenciamento de estacionamentos.

## Instalação

Siga estas instruções para configurar e executar o RIV em seu ambiente local.

### Pré-requisitos

- Python 3.x instalado em seu sistema. Se necessário, faça o download em [python.org](https://www.python.org/downloads/).

### Passos de Instalação

1. Clone este repositório em seu sistema:

   ```bash
   git clone https://gitlab.com/riv-integrado/riv.git
   ```

2. Navegue até o diretório do projeto:

   ```bash
   cd RIV
   ```

3. Instale as dependências:

   ```bash
   pip install -r requirements.txt
   ```

4. Inicie o banco de dados:

   ```bash
   flask db init
   ```
   ```bash
   flask db migrate
   ```
   ```bash
   flask db upgrade
   ```

5. Inicie o aplicativo:

   ```bash
   flask run --host=0.0.0.0
   ```

6. Acesse o RIV no seu navegador em `http://127.0.0.1:5000`.
